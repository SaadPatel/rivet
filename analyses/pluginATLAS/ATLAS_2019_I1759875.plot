# BEGIN PLOT /ATLAS_2019_I1759875/*
XTwosidedTicks=1
YTwosidedTicks=1
LegendYPos=0.90
LegendXPos=0.95
LegendAlign=r
LogY=1
Title=
RatioPlotYMax=1.02
RatioPlotYMin=0.98
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d01
XLabel=Lepton $p_\text{T}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_\text{T}$ [fb/GeV]
# END PLOT
 
# BEGIN PLOT /ATLAS_2019_I1759875/d02
XLabel=Lepton $p_\text{T}$ [GeV]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} p_\text{T}$ [1/GeV]
# END PLOT
 
# BEGIN PLOT /ATLAS_2019_I1759875/d03
LogY=0
XLabel=Lepton $|\eta|$
YLabel=$\text{d}\sigma / \text{d}|\eta|$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d04
LogY=0
XLabel=Lepton $|\eta|$
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d05
XLabel=Dilepton $p_\text{T}^{e\mu}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_\text{T}^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d06
XLabel=Dilepton $p_\text{T}^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} p_\text{T}^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d07
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d08
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} m^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d09
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$\text{d}\sigma / \text{d} |y^{e\mu}|$ [fb]
LegendYPos=0.55
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d10
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} |y^{e\mu}|$
LegendYPos=0.55
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d11
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$\text{d}\sigma / \text{d} \Delta \phi^{e\mu}$ [fb/rad]
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d12
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} \Delta \phi^{e\mu}$ [1/rad]
LegendXPos=0.05
LegendAlign=l
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d13
XLabel=Dilepton $p_\text{T}^{e} + p_\text{T}^{\mu}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} ( p_\text{T}^{e} + p_\text{T}^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d14
XLabel=Dilepton $p_\text{T}^{e} + p_\text{T}^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} ( p_\text{T}^{e} + p_\text{T}^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d15
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} ( E^{e} + E^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d16
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \text{d}\sigma / \text{d} ( E^{e} + E^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d17
LogY=1
XLabel=Lepton $|\eta|\times m^{e\mu}$ 
YLabel=$\text{d}^2\sigma / \text{d}|\eta|\text{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d18
LogY=1
XLabel=Lepton $|\eta|$\times m^{e\mu}$ 
YLabel=$1 / \sigma \; \text{d}^2\sigma / \text{d}|\eta|\text{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d19
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$ 
YLabel=$\text{d}^2\sigma / \text{d}|y^{e\mu}|\text{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d20
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$ 
YLabel=$1 / \sigma \; \text{d}^2\sigma / \text{d}|y^{e\mu}|\text{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d21
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$ 
YLabel=$\text{d}^2\sigma / \text{d}\Delta \phi^{e\mu}\text{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1759875/d22
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$ 
YLabel=$1 / \sigma \; \text{d}^2\sigma / \text{d}\Delta \phi{e\mu}\text{d}m^{e\mu}$
# END PLOT
