BEGIN PLOT /CLEOII_1999_I478217/d01-x01-y01
Title=Spectrum for $\Xi^\prime_c$
XLabel=$x_p$
YLabel=$1/N\text{d}N/\text{d}x_p$
LogY=0
END PLOT
